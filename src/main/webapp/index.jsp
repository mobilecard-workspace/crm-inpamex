<!DOCTYPE html>
<head>
    <title>CRM Inpamex</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <link rel="stylesheet" href="css/floatinglabel.css">
    <script src="js/all.min.js"></script>

</head>

<nav class="navbar navbar-expand-sm menu-head">
    <a class="navbar-brand" href="#">
        <img src="img/mobilecardmx.png" width="30" height="30" alt="">
    </a>
    <!-- Links -->
    <ul class="navbar-nav">
        <!-- <li class="nav-item">
            <a class="nav-link nav-item-link" href="#">Buscar</a>
        </li>
        <li class="nav-item">
            <a class="nav-link nav-item-link" href="#">Cargar Saldo</a>
        </li> -->
    </ul>
</nav>

<body>
<div>
    <div class="container">
        <div id="search" class="search-div">
            <form>
                <div class="row">
                    <p class="title-search">Buscar</p>
                </div>
                <div class="row">
                    <div class="col-4">
                        <input type="text" class="form-control form-control-sm" id="id_mto" placeholder="IDMTO">
                    </div>
                </div>

                <div class="row div-toggle" id="show-div-benef">
                    <p class="title-search" style="padding-top: 10px;">Beneficiario/Sender</p>
                </div>
                <div class="row" id="div-search-benef">
                    <div class="col">
                        <input type="text" class="form-control form-control-sm" id="name" placeholder="Nombre">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control form-control-sm" id="paterno" placeholder="Apellido Paterno">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control form-control-sm" id="materno" placeholder="Apellido Materno">
                    </div>
                </div>
                <div class="row button-search-div" style="padding-top: 10px;">
                    <button id="search-button" type="button" class="btn btn-primary button-search">Buscar</button>
                </div>
                <div class="row" id="validCard" style="display: none;">
                    <div class="col-4">
                        <input type="text" class="form-control form-control-sm" id="number_card" placeholder="Tarjeta">
                    </div>
                    <div class="col-1">
                        <button id="valid-button" type="button" class="btn btn-primary button-search">Validar</button>
                    </div>
                    <div class="col-2">
                        <button id="abonar-button" type="button" class="btn btn-primary button-search" disabled>Cargar Saldo</button>
                    </div>
                </div>
                <div class="row">
                    <div class="alert alert-danger alert-msg" role="alert" id="messageError" style="display: none;"></div>
                </div>
            </form>
        </div>

        <div id="div-result" style="margin-top: 25px; overflow-x: scroll; display: none;">
            <div class="row">
                <p class="title-search">Transacci&oacute;n</p>
                <table class="table table-striped table-sm table-res table-hover" style="margin-left: 25px;">
                    <thead class="thead-dark">
                        <tr>
                            <th>Id Mto</th>
                            <th>Monto original</th>
                            <th>Monto final</th>
                            <th>Tipo de cambio</th>
                            <th>Fecha</th>
                            <th>Estatus</th>
                            <th>Descripcion estatus</th>
                        </tr>
                    </thead>
                    <tbody id="body_transaction"></tbody>
                </table>
            </div>
            <div class="row">
                <p class="title-search">Remitente</p>
                <!-- table remitente-->
                <table class="table table-striped table-sm table-res table-hover" style="margin-left: 25px;">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido paterno</th>
                            <th>Apellido materno</th>
                            <th>Pais</th>
                            <th>Ciudad</th>
                            <th>Estado</th>
                            <th>Direccion</th>
                            <th>CP</th>
                            <th>Telefono</th>
                        </tr>
                    </thead>
                    <tbody id="body_remitente"></tbody>
                </table>
            </div>
            <div class="row">
                <p class="title-search">Ingrese datos beneficiario</p>
            </div>
            <div class="row">
                <label for="nombre">Nombres</label>
                <input type="text" class="form-control" id="nombre">
                <label for="apPaterno">Apellido paterno</label>
                <input type="text" class="form-control" id="apPaterno">
                <label for="apMaterno">Apellido materno</label>
                <input type="text" class="form-control" id="apMaterno">
            </div>
                <!-- table class="table table-striped table-sm table-res table-hover" style="margin-left: 25px;">
                    <tr>
                        <td>Nombre: </td>
                        <td><input type="text" class="form-control form-control-sm" id="nombre" placeholder="Nombre"></td>
                        <td>Apellido paterno: </td>
                        <td><input type="text" class="form-control form-control-sm" id="apPaterno" placeholder="Apellido paterno"></td>
                        <td>Apellido Materno: </td>
                        <td><input type="text" class="form-control form-control-sm" id="apMaterno" placeholder="Apellido materno"></td>
                    </tr>
                    <tr>
                        <td>Fecha de nacimiento: </td>
                        <td><input type="text" class="form-control form-control-sm" id="fechaNac" placeholder="Fecha de nacimiento"></td>
                        <td>Pais: </td>
                        <td><input type="text" class="form-control form-control-sm" id="pais" placeholder="Pais"></td>
                        <td>Estado: </td>
                        <td><input type="text" class="form-control form-control-sm" id="estado" placeholder="Estado"></td>
                    </tr>
                    <tr>
                        <td>Genero: </td>
                        <td>
                            <select name="sexo" id="sexo">
                            <option value="H">Masculino</option>
                            <option value="M">Femenino</option>
                          </select>
                        </td>
                    </tr>
                </table -->
            <div class="row">
                <p class="title-result">Ingrese datos de domicilio</p>
                <table class="table table-striped table-sm table-res table-hover" style="margin-left: 25px;">
                    <tr>
                        <td>Pais: </td>
                        <td><input type="text" class="form-control form-control-sm" id="pais" placeholder="Pais"></td>
                        <td>Estado: </td>
                        <td><input type="text" class="form-control form-control-sm" id="estado" placeholder="Estado"></td>
                        <td>Calle: </td>
                        <td><input type="text" class="form-control form-control-sm" id="calle" placeholder="Calle"></td>
                    </tr>
                    <tr>
                        <td>Numero exterior: </td>
                        <td><input type="text" class="form-control form-control-sm" id="noExterior" placeholder="Numero exterior"></td>
                        <td>Numero interior: </td>
                        <td><input type="text" class="form-control form-control-sm" id="noInterior" placeholder="Numero interior"></td>
                        <td>Municipio: </td>
                        <td><input type="text" class="form-control form-control-sm" id="municipio" placeholder="Municipio"></td>
                    </tr>
                    <tr>
                        <td>Colonia: </td>
                        <td><input type="text" class="form-control form-control-sm" id="colonia" placeholder="Colonia"></td>
                        <td>Codigo Postal: </td>
                        <td><input type="text" class="form-control form-control-sm" id="cp" placeholder="cp"></td>
                        <td>Ciudad: </td>
                        <td><input type="text" class="form-control form-control-sm" id="ciudad" placeholder="Ciudad"></td>
                    </tr>
                    <tr>
                        <td>Estado: </td>
                        <td><input type="text" class="form-control form-control-sm" id="estado" placeholder="Estado"></td>
                        <td>Pais: </td>
                        <td><input type="text" class="form-control form-control-sm" id="pais" placeholder="Pais"></td>
                    </tr>
                </table>
            </div>   
            <div class="row">
                <p class="title-result">Ingrese adicionales</p> 
                <table class="table table-striped table-sm table-res table-hover" style="margin-left: 25px;">      
                    <tr>
                        <td>Telefono: </td>
                        <td><input type="text" class="form-control form-control-sm" id="telefono" placeholder="Telefono"></td>
                        <td>CURP: </td>
                        <td><input type="text" class="form-control form-control-sm" id="curp" placeholder="CURP"></td>
                        <td>RFC: </td>
                        <td><input type="text" class="form-control form-control-sm" id="rfc" placeholder="RFC"></td>
                    </tr>
                    <tr>
                        <td>Email: </td>
                        <td><input type="text" class="form-control form-control-sm" id="email" placeholder="Email"></td>
                        <td>Clave Actividade economica: </td>
                        <td><input type="text" class="form-control form-control-sm" id="claveActividadEconomica" placeholder="Clave Actividad Economica"></td>
                        <td>Tipo documento: </td>
                        <td><input type="text" class="form-control form-control-sm" id="tipoDocumento" placeholder="Tipo Documento"></td>
                    </tr>
                    <tr>
                        <td>Numero documento: </td>
                        <td><input type="text" class="form-control form-control-sm" id="noDocumento" placeholder="Numero Documento"></td>
                    </tr>
                </table>
            </div>
            <div class="row button-search-div" style="padding-top: 10px;">
                <button id="add-user" type="button" class="btn btn-primary button-search">Agregar Usuario</button>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/application.js"></script>
<script src="js/jquery.floatinglabel.js"></script>
<script>
	$('input').floatinglabel({
        ignoreId        : ['recaptcha', 'imagefield'],
        animationIn     : {top: '-25px', opacity: '1', left: '25px'},
        animationOut    : {top: '0', opacity: '0', left: '0'},
        delayIn         : 1500,
        delayOut        : 200,
        easingIn        : 'easeOutElastic',
        easingOut       : 'easeOutCirc',
        labelClass      : 'goodbye-placeholder',
        pinClass        : 'dealt-with-it'
    });
</script>
</body>
</html>