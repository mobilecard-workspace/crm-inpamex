$(document).ready(function () {
    const urlHost= 'http://18.224.174.112/';
    const urlContextPath= 'Inpamex/';
    let dropdownNacionalidad = $('#nacionalidad');
    let dropdownPais = $('#pais');
    let dropdownTipoDocumento = $('#tipoDocumento');
    let dropdownActividadEconomica = $('#claveActividadEconomica');
    let usuarioGuardado;
    let usuarioNombre;
    let tarjetaId;
    let tarjetaNumero;
    let id_Mto_flag;
    dropdownNacionalidad.empty();
    dropdownNacionalidad.append('<option selected="true" disabled> -- Seleccione --</option>');
    dropdownNacionalidad.prop('selectedIndex', 0);
    dropdownPais.empty();
    dropdownPais.append('<option selected="true" disabled> -- Seleccione --</option>');
    dropdownPais.prop('selectedIndex', 0);
    dropdownTipoDocumento.empty();
    dropdownTipoDocumento.append('<option selected="true" disabled> -- Seleccione --</option>');
    dropdownTipoDocumento.prop('selectedIndex', 0);
    dropdownActividadEconomica.empty();
    dropdownActividadEconomica.append('<option selected="true" disabled> -- Seleccione --</option>');
    dropdownActividadEconomica.prop('selectedIndex', 0);
    const urlNacionalidad = 'api/catalogo/nacionalidad/consulta';
    const urlPaises = 'api/catalogo/paises/consulta';
    const urlTipoDocumentos = 'api/catalogo/tipoDocumento/consulta';
    const urlActividadEconomica = 'api/catalogo/actividadEconomica/consulta';
    $('#adduser').hide();
    $('#search-tarjeta').hide();
    $('#formTransaccion').hide();
    $('#formCargarSaldo').hide();
    $('#cargar-saldo').hide();
    $('#enrolar-card').hide();
    $('#reporte-usuarios').hide();
    $('#reporte-transacciones').hide();
    $('#usuario-encontrado').hide();
    //Me arroja las propiedades del objeto: console.log(Object.getOwnPropertyNames(respuesta))
    $.ajax({
        url: urlHost + urlContextPath + urlNacionalidad,
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
        },
        success: function (respuesta) {
            $.each(respuesta.response, function (key, entry) {
                dropdownNacionalidad.append($('<option></option>').attr('value', entry.clave).text(entry.descripcion));
              })
        },
        error: function () {
            showMessageError('Ocurrio un error al realizar la busqueda.');
        }
    });
    $.ajax({
        url: urlHost + urlContextPath + urlPaises,
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
        },
        success: function (respuesta) {
            $.each(respuesta.response, function (key, entry) {
                dropdownPais.append($('<option></option>').attr('value', entry.clave).text(entry.descripcion));
              })
        },
        error: function () {
            showMessageError('Ocurrio un error al realizar la busqueda.');
        }
    });
     $.ajax({
         url: urlHost + urlContextPath + urlTipoDocumentos,
         type: "GET",
         beforeSend: function (xhr) {
             xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
         },
         success: function (respuesta) {
             $.each(respuesta.response, function (key, entry) {
                 dropdownTipoDocumento.append($('<option></option>').attr('value', entry.clave).text(entry.descripcion));
               })
         },
         error: function () {
             showMessageError('Ocurrio un error al realizar la busqueda.');
         }
     });
     $.ajax({
              url: urlHost + urlContextPath + urlActividadEconomica,
              type: "GET",
              beforeSend: function (xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
              },
              success: function (respuesta) {
                  $.each(respuesta.response, function (key, entry) {
                      dropdownActividadEconomica.append($('<option></option>').attr('value', entry.clave).text(entry.descripcion));
                    })
              },
              error: function () {
                  showMessageError('Ocurrio un error al realizar la busqueda.');
              }
          });

    /**
    * show/hide div search beneficiario
    */
    $("#show-div-benef").click(function () {
        $("#div-search-benef").toggle(500);
        $("#benef-name").focus();
    });
//Tabs
	$('ul.tabs li a:first').addClass('active');
	$('.secciones article').hide();
	$('.secciones article:first').show();

//funcion para tabs
	$('ul.tabs li a').click(function(){
		$('ul.tabs li a').removeClass('active');
		$(this).addClass('active');
		$('.secciones article').hide();

		var activeTab = $(this).attr('href');
		$(activeTab).show();
		return false;
	});



    $('#search-button').click(function () {
            //validar si el campo idMto tiene informacion, buscar por este
            if (!isEmpty($('#id_mto').val())) {
                $('#search-button').prop('disabled', true);
                var data = {
                    "idMto": $('#id_mto').val()
                }

                $.ajax({
                    url: 'https://www.mobilecard.mx/Inpamex/api/transferencia/consulta',
                    type: "GET",
                    data: data,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
                    },
                    success: function (respuesta) {
                        //console.log(JSON.stringify(respuesta));
                        if(respuesta.idError != 0){
                            showMessageError(respuesta.mensajeError);
                        } else {
                            showMessageSuccess('Tranferencia localizada');
                            $('#search').hide();
                            $('#formTransaccion').show();
                            printResponseSearch(respuesta);
                            if(respuesta.estatus != 'T'){
                                $('#adduser').show();
                                $('#nombre').val(respuesta.beneficiarioNombre);
                                $('#apPaterno').val(respuesta.beneficiarioApaterno);
                                $('#apMaterno').val(respuesta.beneficiarioAmaterno);
                                $('#usuario-saldo').val(respuesta.beneficiarioNombre + " " + respuesta.beneficiarioApaterno + " " + respuesta.beneficiarioAmaterno);
                                $('#usuario-remitente').val(respuesta.remitenteNombre + " " + respuesta.remitenteApaterno + " " + respuesta.remitenteAmaterno);
                                $('#saldo').val("$ " + respuesta.montoFinal);
                                $('#validar-usuario').show();
                                $('#form-user').hide();
                                $('#validar-user').hide();
                                id_Mto_flag=respuesta.idMto;
                                $('#sender').val(respuesta.remitenteNombre + " " + respuesta.remitenteApaterno);
                                $('#id_Mto_flag').val(id_Mto_flag);
                                $('#continuar').show();
                            }
                            else{
                                ShowMessageWarning(' El saldo ya fue agregado');
                                $('#Actualizar3').show();
                            }
                        }
                        $('#search-button').prop('disabled', false);
                    },
                    error: function () {
                        $('#search-button').prop('disabled', false);
                        showMessageError('Ocurrio un error al realizar la busqueda.');
                    }
                });

            } else {
                    showMessageError('Debe ingresar el identificador de la transferencia.');
                }
        });


    $('#add-user').click(function () {
        //validar campos
        if (validateUserFields()!=false) {
            /*$('#add-user').prop('disabled', true);*/
            var data = {
                "nombre": $('#nombre').val(),
                "apaterno": $('#apPaterno').val(),
                "amaterno": $('#apMaterno').val(),
                "fechaNacimiento": $('#fechaNacimiento').val(),
                "nacionalidad": 1,
                "paisNacimiento": $('#pais').val(),
                "estadoNacimiento": $('#estado').val(),
                "genero": $('#sexo').val(),
                "domicilio": {
                    "calle": $('#calle').val(),
                    "noExterior": $('#noExterior').val(),
                    "noInterior": $('#noInterior').val(),
                    "municipio": $('#municipio').val(),
                    "colonia": $('#colonia').val(),
                    "cp": $('#cp').val(),
                    "ciudad": $('#ciudad').val(),
                    "estado": $('#estado').val(),
                    "pais": $('#pais').val(),
                },
                "telefono": $('#telefono').val(),
                "curp": $('#curp').val(),
                "email": $('#email').val(),
                "claveActividadEconomica": $('#claveActividadEconomica').val(),
                "rfc": $('#rfc').val(),
                "tipoDocumento": $('#tipoDocumento').val(),
                "noDocumento": $('#noDocumento').val()
            }
            console.log(JSON.stringify(data));
            $.ajax({
                url: 'https://www.mobilecard.mx/Inpamex/api/usuario/nuevo',
                type: "POST",
                contentType: 'application/json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
                },
                success: function (respuesta) {
                    $('#Actualizar').hide();
                    $('#add-user').prop('disabled', true);
                    //console.log('usuario guardado exitosamente');
                    usuarioGuardado = respuesta.idUsuario;
                    usuarioNombre = respuesta.nombre + " " + respuesta.apaterno;
                    $('#usuarioGuardado').val(usuarioGuardado);
                    $('#usuarioNombre').val(respuesta.nombre + " " + respuesta.apaterno);
                    $('#beneficiario').val(respuesta.nombre + " " + respuesta.apaterno);
                    $('#nombre').prop('disabled', true);$('#apPaterno').prop('disabled', true);$('#apMaterno').prop('disabled', true);
                    $('#fechaNacimiento').prop('disabled', true);$('#pais').prop('disabled', true);$('#estado').prop('disabled', true);
                    $('#sexo').prop('disabled', true);$('#pais').prop('disabled', true);$('#calle').prop('disabled', true);
                    $('#noExterior').prop('disabled', true);$('#noInterior').prop('disabled', true);$('#municipio').prop('disabled', true);
                    $('#colonia').prop('disabled', true);$('#cp').prop('disabled', true);$('#ciudad').prop('disabled', true);
                    $('#estado').prop('disabled', true);$('#telefono').prop('disabled', true);$('#curp').prop('disabled', true);
                    $('#email').prop('disabled', true);$('#claveActividadEconomica').prop('disabled', true);$('#rfc').prop('disabled', true);
                    $('#tipoDocumento').prop('disabled', true);$('#noDocumento').prop('disabled', true);$('#nacionalidad').prop('disabled', true);
                    /*$('#div-result').hide();*/
                    showMessageSuccess("Usuario agregado exitosamente");
                    $('#search-tarjeta').show();
                    $('#formTarjeta').show();
                    $('#continuar2').show();
                },
                error: function (respuesta) {
                    //console.log(JSON.stringify(respuesta));
                    showMessageError(JSON.stringify(respuesta.responseJSON.errors));
                }
            });
        } else {
            showMessageError('Todos los campos son requeridos.');
        }
    });

    $('#saldo-button').click(function () {
        //validar campos
        var data = {
            "idMto": $('#id_Mto_flag').val(),
            "idUsuario": $('#usuarioGuardado').val()
        }
        //console.log(JSON.stringify(data));
        $.ajax({
            url: 'https://www.mobilecard.mx/Inpamex/api/transferencia/abonar',
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
            },
            success: function (respuesta) {
                showMessageSuccess("El saldo ha sido agregado exitosamente");
                $('#saldo-button').hide();
                $('#Actualizar2').show();
            },
            error: function (respuesta) {
                //console.log(JSON.stringify(respuesta));
                //console.log(Object.getOwnPropertyNames(respuesta))
                showMessageError(JSON.stringify(respuesta.responseJSON.mensajeError));
                $('#saldo-button').hide();
                $('#Actualizar2').show();
            }
        });
    });


    $('#valid-button').click(function () {
        var card = $('#tarjeta').val();
        $('#messageError').hide();
        if (!isEmpty(card)) {
            var data = {
                numero: card
            }
            $.ajax({
                url: 'https://www.mobilecard.mx/Inpamex/api/tarjeta/consulta/numero',
                type: "GET",
                data: data,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
                },
                success: function (response) {
                    if(response.idError < 0){
                        showMessageError("Tarjeta no registrada");
                        $('#Finalizar1').show();
                    } else {
                        //console.log(Object.getOwnPropertyNames(response))
                        showMessageSuccess('Tarjeta localizada en sistema');
                        tarjetaId=response.idTarjeta;
                        var data1 ={
                            "idUsuario": usuarioGuardado,
                            "idTarjeta": tarjetaId
                         }
                            //console.log(JSON.stringify(data1));
                            $.ajax({
                            url: 'https://www.mobilecard.mx/Inpamex/api/tarjeta/asignar',
                            type: "POST",
                            contentType: 'application/json',
                            data: JSON.stringify(data1),
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
                            },
                                success: function (respuesta) {
                                //console.log('Tarjeta asignada exitosamente');
                                    $('#formCargarSaldo').show();
                                    $('#cargar-saldo').show();
                                    $('#valid-button').prop('disabled', true);
                                    showMessageSuccess("Tarjeta validada y asignada correctamente");
                                    $('#continuar3').show();
                                },
                            error: function (respuesta) {
                                //console.log(JSON.stringify(respuesta));
                                //$('#valid-button').hide();
                                showMessageError(respuesta.responseJSON.mensajeError);
                                $('#Finalizar1').show();
                            }
                         });
                        tarjetaNumero=$('#numero-tarjeta').val();
                        $('#valid-button').prop('disabled', true);
                    }
                },
                error: function (response) {
                    //console.log(JSON.stringify(response));
                    showMessageError(JSON.stringify(response.statusText));
                    $('#valid-button').hide();
                    $('#Finalizar1').show();
                }
            });
        }else{
                showMessageError('Debe ingresar un numero de tarjeta valido');
        }
    });


    $('#valid-user').click(function () {
        var curp = $('#validar-curp').val();
        $('#messageError').hide();
        if (!isEmpty(curp)) {
            var data = {
                curp: curp
            }
            $.ajax({
                url: 'https://www.mobilecard.mx/Inpamex/api/usuario/consultaCurp',
                type: "GET",
                data: data,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
                },
                success: function (response) {
                    if(response.usuario == null){
                        showMessageSuccess('Registrar Usuario');
                        //console.log(curp);
                        $('#curp').val(curp);
                        $('#rfc').val(curp.substr(0,13));
                        $('#adduser').show();
                        $('#form-user').show();
                        $('#valid-user').prop('disabled', true);
                    } else {
                        showMessageSuccess("Usuario encontrado en el sistema");
                        //console.log(response.usuario.idUsuario);
                        //console.log(response.usuario.nombre);
                        usuarioGuardado = response.usuario.idUsuario;
                        $('#usuarioGuardado').val(usuarioGuardado);
                        usuarioNombre = response.usuario.nombre + " " + response.usuario.apaterno;
                        $('#validar-user').val(JSON.stringify(response.usuario, undefined, 4));
                        /*
                        $('#adduser').show();*/
                        $('#usuario-encontrado').show();
                        $('#validar-user').show();
                        $('#valid-user').prop('disabled', true);
                        $('#beneficiario').val(response.usuario.nombre + " " + response.usuario.apaterno);
                        $('#perdida-tarjeta').show();
                        $('#continuar1').show();
                    }
                },
                error: function (response) {
                    showMessageError(response.mensajeError);
                }
            });
        }else{
            showMessageError('Debe ingresar un CURP');
        }
    });

    function printListResponseSearch(respuesta) {
        $('#search-button').prop('disabled', false);
        $('#body_user_inpa').html('');
        $('#body_sucursal').html('');
        $('#body_remitente').html('');
        $('#body_beneficiario').html('');
        $('#body_transaction').html('');

        for (var i = 0; i < respuesta.transferencias.length; i++) {
            $('#body_user_inpa').html($('#body_user_inpa').html() + createTableUserInpamex(respuesta.transferencias[i].usuariosInpamex));
            $('#body_remitente').html($('#body_remitente').html() + createTableRemintente(respuesta.transferencias[i]));
            $('#body_beneficiario').html($('#body_beneficiario').html() + createTableBeneficiario(respuesta.transferencias[i]));
            $('#body_transaction').html($('#body_transaction').html() + createTableTransaction(respuesta.transferencias[i]));
        }
        $('#div-result').show();
        $('#validCard').show();
    }

    function printResponseSearch(respuesta) {
        $('#search-button').prop('disabled', false);
        $('#body_user_inpa').html('');
        $('#body_sucursal').html('');
        $('#body_remitente').html('');
        $('#body_beneficiario').html('');
        $('#body_transaction').html('');
        console.log('Datos: '+respuesta.idTransferencia);
        $('#body_remitente').html($('#body_remitente').html() + createTableRemintente(respuesta));
        $('#body_transaction').html($('#body_transaction').html() + createTableTransaction(respuesta));

        //$('#body_user_inpa').html($('#body_user_inpa').html() + createTableUserInpamex(respuesta));
        //$('#body_sucursal').html($('#body_sucursal').html() + createTableSucursal(respuesta));
        //$('#body_beneficiario').html($('#body_beneficiario').html() + createTableBeneficiario(respuesta));

        $('#div-result').show();
        $('#validCard').show();
    }

    function createTableTransaction(transaction) {
        var body = '<tr>';
        if (transaction !== undefined) {
            body += '<td>' + transaction.idMto + '</td>';
            body += '<td>' + '$ ' +transaction.montoOriginal + '</td>';
            body += '<td>' + '$ ' +transaction.montoFinal + '</td>';
            body += '<td>' + '$ ' +transaction.tipoCambio + '</td>';
            body += '<td>' + transaction.fecha + '</td>';
            body += '<td>' + transaction.estatus + '</td>';
            body += '<td>' + transaction.estatusDescripcion + '</td>';
            body += '</tr>';
            return body;
        }
    }

    function createTableBeneficiario(beneficiario) {
        var body = '<tr>';
        if (beneficiario != undefined) {
            body += '<td>' + beneficiario.beneficiarioNombre + '</td>';
            body += '<td>' + beneficiario.beneficiarioApaterno + '</td>';
            body += '<td>' + beneficiario.beneficiarioAmaterno + '</td>';
            body += '<td>' + beneficiario.beneficiarioPais + '</td>';
            body += '<td>' + beneficiario.beneficiarioEstado + '</td>';
            body += '<td>' + beneficiario.beneficiarioCiudad + '</td>';
            body += '<td>' + beneficiario.beneficiarioCp + '</td>';
            body += '<td>' + beneficiario.beneficiarioCalle + '</td>';
            body += '<td>' + beneficiario.beneficiarioNoExterior + '</td>';
            body += '<td>' + beneficiario.beneficiarioNoInterior + '</td>';
            body += '<td>' + beneficiario.beneficiarioTelefono + '</td>';
            body += '<td>' + beneficiario.beneficiarioEmail + '</td>';
            body += '</tr>';
            return body;
        }
    }

    function createTableRemintente(remitente) {
        var body = '<tr>';
        console.log('Direccion: '+remitente.remitenteDireccion);
        if (remitente !== undefined) {
            if (remitente.remitenteNombre != 'undefined'){
                body += '<td>' + remitente.remitenteNombre + '</td>';
            } else {
                body += '<td> </td>';
            }
            if (remitente.remitenteApaterno != 'undefined'){
                body += '<td>' + remitente.remitenteApaterno + '</td>';
            } else {
                body += '<td> </td>';
            }
            if (remitente.remitenteAmaterno != 'undefined'){
                body += '<td>' + remitente.remitenteAmaterno + '</td>';
            } else {
                body += '<td> </td>';
            }
            if (remitente.remitenteCiudad != 'undefined'){
                body += '<td>' + remitente.remitenteCiudad + '</td>';
            } else {
                body += '<td> </td>';
            }
            if (remitente.remitenteEstado != 'undefined' || remitente.remitenteEstado != NULL){
                body += '<td>' + remitente.remitenteEstado + '</td>';
            } else {
                body += '<td> </td>';
            }
            if (remitente.remitenteDireccion != 'undefined'){
                body += '<td>' + remitente.remitenteDireccion + '</td>';
            } else {
                body += '<td> </td>';
            }
            if (remitente.remitenteCp != 'undefined'){
                body += '<td>' + remitente.remitenteCp + '</td>';
            } else {
                body += '<td> </td>';
            }
            if (remitente.remitenteTelefono != 'undefined'){
                body += '<td>' + remitente.remitenteTelefono + '</td>';
            } else {
                body += '<td> </td>';
            }
            if (remitente.remitenteEmail != 'undefined'){
                body += '<td>' + remitente.remitenteEmail + '</td>';
            } else {
                body += '<td> </td>';
            }
            body += '</tr>';
            return body;
        }
    }

    function createTableSucursal(transferencias) {
        var body = '<tr>';
        if (transferencias !== undefined) {
            body += '<td>' + transferencias.sucursalNombre + '</td>';
            body += '<td>' + transferencias.sucursalClave + '</td>';
            body += '<td>' + transferencias.sucursalPais + '</td>';
            body += '<td>' + transferencias.sucursalCiudad + '</td>';
            body += '<td>' + transferencias.sucursalEstado + '</td>';
            body += '<td>' + transferencias.sucursalDireccion + '</td>';
            body += '<td>' + transferencias.sucursalCp + '</td>';
            body += '</tr>';
            return body;
        }
    }

    function createTableUserInpamex(usuarioInpamex) {
        var body = '<tr>';
        if (usuarioInpamex !== undefined) {

            body += '<td>' + usuarioInpamex.idUsuario + '</td>';
            body += '<td>' + usuarioInpamex.remitenteNombre + '</td>';
            body += '<td>' + usuarioInpamex.remitenteApaterno + '</td>';
            body += '<td>' + usuarioInpamex.remitenteAmaterno + '</td>';
            body += '<td>' + usuarioInpamex.fecha + '</td>';
            body += '<td>' + usuarioInpamex.estatus + '</td>';
            body += '<td>' + usuarioInpamex.comentario + '</td>';

            /** body += '<td>' + usuarioInpamex.idTarjeta + '</td>';
            body += '<td>' + usuarioInpamex.idMto + '</td>';
            body += '<td>' + usuarioInpamex.noAutorizacionTransferencia + '</td>';
            body += '<td>' + usuarioInpamex.estatus + '</td>';
            body += '<td>' + usuarioInpamex.estatusDescripcion + '</td>';
            body += '<td>' + usuarioInpamex.mensajeError + '</td>'; **/
            body += '</tr>';
            return body;
        }
    }

    function validateUserFields() {
        var nombre = $('#nombre').val();
        var paterno = $('#apPaterno').val();
        var materno = $('#apMaterno').val();
        var fechaNac = $('#fechaNac').val();
        var pais = $('#pais').val();
        var estado = $('#estado').val();
        var sexo = $('#sexo').val();
        var pais = $('#pais').val();
        var calle = $('#calle').val();
        var noExterior = $('#noExterior').val();
        var noInterior = $('#noInterior').val();
        var municipio = $('#municipio').val();
        var colonia = $('#colonia').val();
        var cp = $('#cp').val();
        var ciudad = $('#ciudad').val();
        var estado = $('#estado').val();
        var telefono = $('#telefono').val();
        var curp = $('#curp').val();
        var email = $('#email').val();
        var claveActividadEconomica = $('#claveActividadEconomica').val();
        var rfc = $('#rfc').val();
        var tipoDocumento = $('#tipoDocumento').val();
        var noDocumento = $('#noDocumento').val();
        console.log('Validando campos - Nombre: '+isEmpty(nombre));
        console.log('Validando campos - paterno: '+isEmpty(paterno));
        console.log('Validando campos - materno: '+isEmpty(materno));
        console.log('Validando campos - fechaNac: '+isEmpty(fechaNac));
        console.log('Validando campos - pais: '+isEmpty(pais));
        console.log('Validando campos - estado: '+isEmpty(estado));

        console.log('Validando campos - sexo: '+isEmpty(sexo));
        console.log('Validando campos - calle: '+isEmpty(calle));
        console.log('Validando campos - noExterior: '+isEmpty(noExterior));
        console.log('Validando campos - noInterior: '+isEmpty(noInterior));
        console.log('Validando campos - municipio: '+isEmpty(municipio));
        console.log('Validando campos - colonia: '+isEmpty(colonia));

        console.log('Validando campos - cp: '+isEmpty(cp));
        console.log('Validando campos - ciudad: '+isEmpty(ciudad));
        console.log('Validando campos - estado: '+isEmpty(estado));
        console.log('Validando campos - telefono: '+isEmpty(telefono));
        console.log('Validando campos - curp: '+isEmpty(curp));
        console.log('Validando campos - email: '+isEmpty(email));

        console.log('Validando campos - claveActividadEconomica: '+isEmpty(claveActividadEconomica));
        console.log('Validando campos - rfc: '+isEmpty(rfc));
        console.log('Validando campos - tipoDocumento: '+isEmpty(tipoDocumento));
        console.log('Validando campos - noDocumento: '+isEmpty(noDocumento));
        if (isEmpty(nombre) && isEmpty(paterno) && isEmpty(materno)
            && isEmpty(fechaNac) && isEmpty(pais) && isEmpty(estado)
            && isEmpty(calle) && isEmpty(noExterior)
            && isEmpty(noInterior) && isEmpty(municipio) && isEmpty(colonia)
            && isEmpty(cp) && isEmpty(ciudad) && isEmpty(estado)
            && isEmpty(telefono) && isEmpty(curp) && isEmpty(email)
            && isEmpty(claveActividadEconomica) && isEmpty(rfc) && isEmpty(tipoDocumento)
            && isEmpty(noDocumento)) {
            console.log('Campos no validos.');
            return false;
        } else {
        console.log('Campos validos');
            return true;
        }
    }

    function validateCardFields(){
        var numero = $('#numero-tarjeta').val();
        var codigoBarras = $('#codigo-barras').val();
        var codigoVerificacion = $('#codigo-verificacion').val();
        var claveCadena = $('#clave-cadena').val();
        console.log('Validando campos - Numero de tarjeta: '+isEmpty(numero));
        console.log('Validando campos - Codigo de Barras: '+isEmpty(codigoBarras));
        console.log('Validando campos - Codigo de Verificacion: '+isEmpty(codigoVerificacion));
        console.log('Validando campos - Clave Cadena: '+isEmpty(claveCadena));
        if (isEmpty(numero) && isEmpty(codigoBarras) && isEmpty(codigoVerificacion) && isEmpty(claveCadena)){
            console.log('Campos no validos');
            return false;
        } else{
            console.log('Campos validos');
            return true;
        }
    }


    function isEmpty(value) {
        if (value === undefined || value === '') {
            return true;
        } else {
            return false;
        }
    }

    function showMessageError(message) {
        $('#messageError').html('<strong>Error </strong>' + message);
        $('#messageError').show(100);
        setTimeout(function() {
                        $('#messageError').hide();
                },5000);
    }
    function showMessageSuccess(message) {
        $('#messageSuccess').html('<strong>Enhorabuena! </strong>' + message);
        $('#messageSuccess').show(100);
        setTimeout(function() {
                $('#messageSuccess').hide();
        },2000);

        }
     function ShowMessageWarning(message) {
             $('#messageWarning').html('<strong>Advertencia! </strong>' + message);
             $('#messageWarning').show(100);
             setTimeout(function() {
                     $('#messageWarning').hide();
             },4000);

             }


        $('#Actualizar').click(function (){
            Finalizar();
        });

        $('#Actualizar2').click(function (){
           Finalizar();
        });
        $('#Actualizar3').click(function (){
           Finalizar();
        });
        $('#Actualizar4').click(function (){
                   Finalizar();
        });
        $('#Finalizar').click(function (){
                   Finalizar();
        });
        $('#Finalizar1').click(function (){
                   Finalizar();
        });
        $('#Finalizar2').click(function (){
                   Finalizar();
        });
        $('#Finalizar3').click(function (){
                   Finalizar();
        });

        function Finalizar() {
            usuarioGuardado=0;
            usuarioNombre=0;
            tarjetaId=0;
            tarjetaNumero=0;
            id_Mto_flag=0;
            location.reload();
        }
});

	$('#continuar').click(function(){
	    $('ul.tabs li a').removeClass('active');
		$('#tabdos').addClass('active');
		$('.secciones article').hide();
		$('#tab2').show();
		return false;
	});
	$('#perdida-tarjeta').click(function(){
    	    $('ul.tabs li a').removeClass('active');
    		$('#tabtres').addClass('active');
    		$('.secciones article').hide();
    		$('#tab3').show();
    		$('#search-tarjeta').show();
            $('#formTarjeta').show();
    		return false;
    	});
    $('#continuar1').click(function(){
        $('ul.tabs li a').removeClass('active');
		$('#tabcuatro').addClass('active');
		$('.secciones article').hide();
		$('#tab4').show();
        $('#formCargarSaldo').show();
        $('#cargar-saldo').show();
		return false;
    	});
    $('#continuar2').click(function(){
	    $('ul.tabs li a').removeClass('active');
		$('#tabtres').addClass('active');
		$('.secciones article').hide();
		$('#tab3').show();
		return false;
	});
    $('#continuar3').click(function(){
	    $('ul.tabs li a').removeClass('active');
		$('#tabcuatro').addClass('active');
		$('.secciones article').hide();
		$('#tab4').show();
		return false;
	});
    function ReporteUsuarios(arreglo){
	    //Arreglo para el parametro que ingresa
	    RegistrosDoc = new Array(arreglo.length);
	    for (let i = 0; i<arreglo.length;i++){
	        RegistrosDoc[0]=['Id Usuario','Nombre','Apellido Paterno','Apellido Materno','Fecha Nacimiento','Nacionalidad','Pais Nacimiento','Estado Nacimiento',
	        'Genero','Calle','NumeroExt','NumeroInt','Municipio','Colonia','Codigo Postal','Ciudad','Estado','Pais','Telefono','CURP','RFC','email','Clave Actividad Economica',
	        'Tipo documento','Numero documento','Fecha de registro','Estatus','Estatus descripcion','Fecha de modificacion'];
	        RegistrosDoc[i+1] = Object.values(arreglo[i]);
	    }
	    //creacion de un nuevo libro
	    var wb = XLSX.utils.book_new();
        //actualizar las propiedades del libro de trabajo como título, tema, autor
        wb.Props = {
                Title: "Registro de usuarios INPAMEX",
                Subject: "Users",
                Author: "Jorge Macias",
                CreatedDate: new Date(2020,07,02)
        };
        //asignar un nuevo nombre de hoja.
        wb.SheetNames.push("USUARIOS_INPAMEX");
        //contenido dentro de la hoja
        var ws_data = RegistrosDoc;//[['hello' , 'world'],['hello1' , 'world1'],['hello2' , 'world2'],['hello3' , 'world3']];//[1,"JOSUE","OCAMPO","NUÑEZ","2020-06-10T05:00:00.000+0000","MEX","CDMX","M","POLEN","22","2","COYOACAN","EL RELOJ","04640","CDMX","CDMX","MEX","5543653531","OANJ830926HMSCXS06","OANJ830926U95","JOSVAI@HOTMAIL.COM","DUEÑO","1","1234556","2020-06-18T19:22:46.000+0000","A","ACTIVO","2020-06-18T19:22:46.000+0000"[arreglo]];
        //crear la hoja de esta matriz
        var ws = XLSX.utils.aoa_to_sheet(ws_data);
        //asigne el objeto de hoja a la matriz Hojas de libro de trabajo.
        wb.Sheets["USUARIOS_INPAMEX"] = ws;
        //exportar el libro de trabajo como binario xlsx.
        var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
        //convertir los datos binarios en octetos
        function s2ab(s) {
                        var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
                        var view = new Uint8Array(buf);  //create uint8array as viewer
                        for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
                        return buf;
        }
        //guardado de archivos para la compatibilidad con varios navegadores.
        $('#reporte-usuarios').click(function(){
               saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'INPAMEXusers.xlsx');
               $('#reporte-usuarios').hide();
               $('#Finalizar').show();
        });
    }



	$('#get-users').click(function () {
            $.ajax({
                url: 'https://www.mobilecard.mx/Inpamex/api/usuario/todo',
                type: "GET",
                contentType: 'application/json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
                },
                success: function (response) {
                    ReporteUsuarios(response.usuario);
                    printListResponseUsuario(response.usuario);
                    $('#get-users').hide();
                    $('#reporte-usuarios').show();
                    $('#Finalizar').show();
                },
                error: function (response) {
                    showMessageError(response);
                }
            });
        });

        function printListResponseUsuario(respuesta) {
            $('#body_usuario').html('');
            for (var i = 0; i < respuesta.length; i++) {
                $('#body_usuario').html($('#body_usuario').html() + createTableUsuario(respuesta[i]));
            }
            $('#div-result1').show();
            //$('#validCard').show();
        }

        function createTableUsuario(usuario) {
            var body = '<tr>';
            if (usuario !== undefined) {
                body += '<td>' + usuario.idUsuario + '</td>';
                body += '<td>' + usuario.nombre + '</td>';
                body += '<td>' + usuario.apaterno + '</td>';
                body += '<td>' + usuario.amaterno + '</td>';
                body += '<td>' + usuario.fechaNacimiento + '</td>';
                body += '<td>' + usuario.telefono + '</td>';
                body += '<td>' + usuario.curp + '</td>';
                body += '<td>' + usuario.rfc + '</td>';
                body += '<td>' + usuario.email + '</td>';
                body += '<td>' + usuario.claveActividadEconomica + '</td>';
                body += '<td>' + usuario.fechaRegistro + '</td>';
                body += '<td>' + usuario.estatusDescripcion + '</td>';
                body += '</tr>';
                return body;
            }
        }

function Solo_Texto(e) {
    var code;
    if (!e) var e = window.event;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    var character = String.fromCharCode(code);
    var AllowRegex  = /^[\ba-zA-Z\s-]$/;
    if (AllowRegex.test(character)) return true;
    return false;
}

function Solo_Numeros(e) {
    var code;
    if (!e) var e = window.event;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    var character = String.fromCharCode(code);
    var AllowRegex  = /^[\b0-9\s-]$/;
    if (AllowRegex.test(character)) return true;
    return false;
}

function Solo_NumerosDieciseis(e) {
    var code;
    if (!e) var e = window.event;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    var character = String.fromCharCode(code);
    var AllowRegex  = /^[\b0-9\s-]$/;
    if (AllowRegex.test(character)) return true;
    return false;
}


	$('#get-saldo').click(function () {
        if (!isEmpty($('#codigo-barras').val()) && ($('#clave-cadena').val())) {
                $('#get-saldo').prop('disabled', true);
                var data = {
                    "codigoBarras": $('#codigo-barras').val(),
                    "claveCadena": $('#clave-cadena').val()
                }
                    $.ajax({
                        url: 'https://www.mobilecard.mx/Inpamex/api/tarjeta/consulta/saldo/codigoBarras',
                        type: "GET",
                        data: data,
                        contentType: 'application/json',
                        beforeSend: function (xhr) {
                             xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
                    },
                    success: function (response) {
                            //console.log(JSON.stringify(response));
                            printListResponseConsultaSaldo(response);
                            $('#get-saldo').hide();
                            $('#Finalizar2').show();
                    },
                    error: function (response) {
                        showMessageError(response);
                    }
                });
            } else {
                       showMessageError('Debe ingresar los datos completos.');
                   }
    });

        function printListResponseConsultaSaldo(respuesta) {
            $('#body_Consultar-Saldo').html('');
            $('#body_Consultar-Saldo').html($('#body_Consultar-Saldo').html() + createTableConsultaSaldo(respuesta));
            $('#div-result2').show();
        }

        function createTableConsultaSaldo(saldo) {
            var body = '<tr>';
            if (saldo !== undefined) {
                body += '<td>' + '$ ' + saldo.saldo + '</td>';
                body += '<td>' + saldo.estatusDescripcion + '</td>';
                body += '</tr>';
                return body;
            }
        }
        function isEmpty(value) {
                if (value === undefined || value === '') {
                    return true;
                } else {
                    return false;
                }
            }

        $('#get-transacciones').click(function () {
            if (!isEmpty($('#transaccion').val())) {
                            $('#get-transacciones').prop('disabled', true);
                            var data = {
                                "idTarjeta": $('#transaccion').val()
                            }
                                $.ajax({
                                    url: 'https://www.mobilecard.mx/Inpamex/api/transaccion/consulta/tarjeta',
                                    type: "GET",
                                    data: data,
                                    contentType: 'application/json',
                                    beforeSend: function (xhr) {
                                         xhr.setRequestHeader("Authorization", "Basic " + btoa('inpamex' + ":" + 'tmvCruMz95Lfa3Jrt4ekXDVfmargQH68'));
                                },
                                success: function (response) {
                                        //console.log(JSON.stringify(response.transacciones));
                                        ReporteTransacciones(response.transacciones);
                                        printListResponseTransaccion(response.transacciones);
                                        $('#get-transacciones').hide();
                                        $('#reporte-transacciones').show();
                                        $('#Finalizar3').show();
                                },
                                error: function (response) {
                                    showMessageError(response);
                                }
                            });
                        } else {
                                   showMessageError('Debe ingresar los datos completos.');
                               }
         });

        function printListResponseTransaccion(respuesta) {
            $('#body_transacciones').html('');
            for (var i = 0; i < respuesta.length; i++) {
                $('#body_transacciones').html($('#body_transacciones').html() + createTableTransaccion(respuesta[i]));
            }
            $('#div-result3').show();
            //$('#validCard').show();
        }

        function createTableTransaccion(transaccion) {
            var body = '<tr>';
            if (transaccion !== undefined) {
                body += '<td>' + transaccion.idMto + '</td>';
                body += '<td>' + transaccion.tipo + '</td>';
                body += '<td>' + '$ ' +transaccion.monto + '</td>';
                body += '<td>' + transaccion.concepto + '</td>';
                body += '<td>' + transaccion.claveCadena + '</td>';
                body += '<td>' + transaccion.fecha + '</td>';
                body += '<td>' + transaccion.estatusDescripcion + '</td>';
                body += '<td>' + transaccion.noAutorizacion + '</td>';
                body += '<td>' + transaccion.comentario + '</td>';
                body += '</tr>';
                return body;
            }
        }

        function showMessageError(message) {
                $('#messageError').html('<strong>Error </strong>' + message);
                $('#messageError').show(100);
                setTimeout(function() {
                                $('#messageError').hide();
                        },5000);
            }



function ReporteTransacciones(arreglo){
	    RegistrosDoc = new Array(arreglo.length);
	    for (let i = 0; i<arreglo.length;i++){
	        RegistrosDoc[0]=['Id Transaccion','Id Usuario','Id Transferencia','Id Tarjeta','Id Mto','Tipo','Monto','Concepto',
	        'Clave Cadena','Fecha','Estatus','Estatus Descripcion','Num. Autorizacion','Comentario'];
	        RegistrosDoc[i+1] = Object.values(arreglo[i]);
	    }
	    var wb = XLSX.utils.book_new();
        wb.Props = {
                Title: "Transacciones INPAMEX",
                Subject: "Transactions",
                Author: "Jorge Macias",
                CreatedDate: new Date(2020,07,02)
        };
        wb.SheetNames.push("TRANSACCIONES_INPAMEX");
        var ws_data = RegistrosDoc;
        var ws = XLSX.utils.aoa_to_sheet(ws_data);
        wb.Sheets["TRANSACCIONES_INPAMEX"] = ws;
        var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
        function s2ab(s) {
                        var buf = new ArrayBuffer(s.length);
                        var view = new Uint8Array(buf);
                        for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
                        return buf;
        }
        $('#reporte-transacciones').click(function(){
               saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'INPAMEXtransactions.xlsx');
               $('#reporte-transacciones').hide();
               $('#Finalizar3').show();
        });
    }